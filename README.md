Script for PRTG „Pause Button" on your desktop
====
With this script you will be able to display a pause button on your desktop. When clicking the button, a specified object will be paused within your PRTG installation.

## Version
Version 1 – 2018-09-18  (Written by anonymous PRTG User)
  Readme and Documentation by Nina Wooten)

## Possible Use Cases
* Pause an object under maintenance without logging into PRTG
* You can also modify the script to resume the object or even to stop it. Here is an example of what the three buttons would then look like: 
![Example](./Images/PauseButton_1.png) 
## Setup instructions
**PLEASE BE AWARE THAT THIS SCRIPT STORES THE PASSHASH IN THE SHORTCUT, WHICH IS NOT RECOMMENDED**

The following is the complete script to create the button:
([Download script](https://gitlab.com/PRTG/Admin-Scripts/prtg-desktoppausebutton/raw/master/PRTG-Desktop-Button.ps1?inline=false))

---

```sh
#[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::ssl3
$_SERVER = "SERVERNAME"
$_USERNAME = "LokalerBenutzer"
$_PASSHASH = "################"
$_OBJECTID = $args[0]
$_URL = "https://" + $_SERVER + "/api/pauseobjectfor.htm?id=" + $_OBJECTID +"&duration=15&username=" + $_USERNAME + "&passhash=" + $_PASSHASH `
$_SERVER = "SERVERNAME"
$_USERNAME = "LokalerBenutzer"
$_PASSHASH = "################"
$_OBJECTID = $args[0]
$_URL = "https://" + $_SERVER + "/api/pauseobjectfor.htm?id=" + $_OBJECTID +"&duration=15&username=" + $_USERNAME + "&passhash=" + $_PASSHASH
 
if (!$args) {
    exit 1
} 
 
$request = [System.Net.WebRequest]::Create($_URL)
$request.Method = "GET"
$request.KeepAlive = $false
$response = $NULL
 
try {
    $response = $request.GetResponse()
    $response.Close()
} catch [System.Net.WebException] {
    #Write-Host "ERROR:" + $_.Exception.Status
    try { $response.Close() } catch { }
    exit 1
}
```
-------

The parameters you need to fill out are: 
![Parameters](./Images/PauseButton_2.png)
1) the URL to your server
2) one user (you could create a maintenance user in your installation for this) and 
3) the passhash

After this all that’s left to do is to add the objectID to the properties of the button:
![Add ObjectID](./Images/PauseButton_3.png)
